import React from 'react';
import {StyleSheet, View, Platform, ScrollView, ListView, Image, Dimensions, Animated, StatusBar} from 'react-native';
import MapView, {Polyline, Marker, AnimatedRegion, PROVIDER_GOOGLE} from 'react-native-maps'
import { Card, ListItem, Input, SearchBar, Button, Divider, Icon, Text  } from 'react-native-elements'
import MapViewDirections from 'react-native-maps-directions';
import { Notifications, Location, Permissions, Constants } from 'expo';
import Swiper from 'react-native-swiper'
import SlidingUpPanel from 'rn-sliding-up-panel';

const LATITUDE_DELTA = 0.009;
const LONGITUDE_DELTA = 0.009;
const LATITUDE = -7.321057;
const LONGITUDE = 112.734399;
const { height } = Dimensions.get("window");

export default class App extends React.Component {

  static defaultProps = {
    draggableRange: { top: height + 180 - 64, bottom: 60 }
  };
  _draggedValue = new Animated.Value(180);

    constructor(props){
        super(props)
        this.state = {
            listTempat: [],
            source:[],
            destination:[],
            informasi:{},
            foto: null,
            namaTempat: null,
            keterangantempat: null,
            region: null,
            isLoading: true,
            lat: -7.3365481,
            long: 112.713068,
            concat: null,
            error: null,
            coords:[],
            x: 'false',
            markerPressed : false,
            latitude: LATITUDE,
            longitude: LONGITUDE,
            routeCoordinates: [],
            coordinate: new AnimatedRegion({
                latitude: LATITUDE,
                longitude: LONGITUDE,
                latitudeDelta: 0,
                longitudeDelta: 0
        })
        };
    }

    componentWillMount() {
      if (Platform.OS === 'Android' && !Constants.isDevice) {
        this.setState({
          errorMessage: 'Oops, this will not work on Sketch in an Android emulator. Try it on your device!',
        });
      } else {
        // Location.setApiKey('AIzaSyA9FobRmxO4zNmTJKK_5Au2J0I6M5UqTWU')
        return fetch('https://www.api.ilmusaya.web.id/public/api/listTempat')
        .then((response) => response.json())
        .then((responseJson) => {
          this.setState({listTempat:responseJson})
          this._getLocationAsync();
        })
        .catch((error) => {
          console.error(error);
        });
      }
    }

    _getLocationAsync = async () => {
      let { status } = await Permissions.askAsync(Permissions.LOCATION);
      if (status !== 'granted') {
        this.setState({
          errorMessage: 'Permission to access location was denied',
        });
      }
      let location = await Location.getCurrentPositionAsync({},error => alert(error.message),{enabledHighAccuracy: true});
      this.setState({ 
        coords : {
          latitude : location.coords.latitude,
          longitude :  location.coords.longitude,
          latitudeDelta: 0.009,
          longitudeDelta: 0.009,
        },
        source:[{
                  latitude:location.coords.latitude,
                  longitude:location.coords.longitude,
                }]
      }, () => {
        // this._getAlamatGmaps()
        console.log(this.state)
      });
    }

  //   async _getLocationAsync() {
  //     navigator.geolocation.watchPosition(position => {
  //             const location = position;
  //             // console.log(position)
  //             let region = {
  //                 latitude: location.coords.latitude,
  //                 longitude: location.coords.longitude,
  //                 latitudeDelta: 0.045,
  //                 longitudeDelta: 0.045,
  //             }
  //             //const array = Object.values(location.coords);
  //             this.setState({region: region, lat:region.latitude, long:region.longitude, source:[{
  //               latitude:position.coords.latitude,
  //               longitude:position.coords.longitude,
  //             }]});
  //             //this.setState({lat:region.latitude, long:region.longitude});
  //         }, error => Alert.alert(error.message),{enabledHighAccuracy: true});
  // }

  //     getMapRegion = () => ({
  //       latitude: this.state.latitude,
  //       longitude: this.state.longitude,
  //       latitudeDelta: LATITUDE_DELTA,
  //       longitudeDelta: LONGITUDE_DELTA
  //     }); 

  //   checkPermissions = async () => {
  //     const { status, expires, permissions } = await Permissions.askAsync(
  //       Permissions.CALENDAR,
  //       Permissions.CONTACTS
  //     );
  //     if (status !== 'granted') {
  //       alert('Hey! You heve not enabled selected permissions');
  //     }else{
  //       //alert('Granted')
  //       console.log('granted')
  //     }
  //   }

  //   componentDidMount(){
  //     // console.log(height)
  //     this.checkPermissions();
  //     this._getLocationAsync();
  //     return fetch('https://www.api.ilmusaya.web.id/public/api/listTempat')
  //     .then((response) => response.json())
  //     .then((responseJson) => {
  //        this.setState({listTempat:responseJson})
  //     })
  //     .catch((error) => {
  //       console.error(error);
  //     });
  //   }

    onPressMarker(item, index){
      let lat = this.state.listTempat[index].latitude_tempat;
      let long = this.state.listTempat[index].longitude_tempat;
      this.setState({ 
        markerPressed : true,
        destination:[{latitude: lat, longitude: long}],
        namaTempat: this.state.listTempat[index].nama_tempat,
        foto: this.state.listTempat[index].foto,
        keterangantempat: this.state.listTempat[index].keterangan_tempat
      }, () => {
        this._panel.show(200)
      })
    }

    
    render() {
    const key = 'AIzaSyCu31iVIKd18ciqdbaxRXtsucgioewxhDY';
    // console.log(this.state)

      return (
        <View style={styles.wrapper}>
          <View style={styles.container}>
            {/* <View style={{ flex:1, backgroundColor:'#fff' }}> */}
            <SearchBar placeholder="Search..." containerStyle={{backgroundColor:'transparent' }} inputContainerStyle={{ backgroundColor:'#cfd8dc' }} platform='ios' />
            {/* <Text style={{ position:'absolute', top:3 }}>
                Jarak {this.state.jarak+' Km'} Durasi :{this.state.durasi+' Min'}
            </Text> */}
                  
            { this.state.coords.length !== 0 ?
              <MapView style={styles.map} 
                initialRegion={{  
                  latitude:this.state.coords.latitude,
                  longitude:this.state.coords.longitude,
                  latitudeDelta: this.state.coords.latitudeDelta,
                  longitudeDelta: this.state.coords.longitudeDelta 
                }} 
                showsTraffic={true}
                zoomEnabled={true} 
                showsMyLocationButton={true} 
                showsCompass={true} 
                minZoomLevel={7}
                toolbarEnabled={true}
              >

                {this.state.listTempat.map( (item, index) => (
                  <MapView.Marker
                  coordinate={{latitude: item.latitude_tempat,
                  longitude: item.longitude_tempat}}
                  title={item.nama_tempat}
                  key={item.id_tempat}
                  description={item.keterangan_tempat}
                  pinColor='#1976d2'
                  onPress={(e) => this.onPressMarker(item,index)}
                  />
                ))}

                {!!this.state.coords.latitude && !!this.state.coords.longitude && 
                  <MapView.Marker
                  coordinate={{
                    "latitude":this.state.coords.latitude,
                    "longitude":this.state.coords.longitude
                  }}
                  title={"Your Location"} pinColor="#DC143C"/>
                }
                <MapViewDirections
                    origin={this.state.source[0]}
                    destination={this.state.destination[0]}
                    apikey={key}
                    strokeWidth={3}
                    strokeColor='blue'
                    optimizeWaypoints={true}
                    onReady={ result => {      
                      this.setState({ informasi:{jarak:result.distance, durasi:result.duration}})
                    }}
                />
            </MapView>

            : <View>
              {
                alert(
                  'Error',
                  String(this.state.errorMessage),
                  {cancelable: false},
                )
              }
            </View> }
            
            { this.state.markerPressed === true ?
              <SlidingUpPanel
              ref={c => (this._panel = c)}
              draggableRange={this.props.draggableRange}
              animatedValue={this._draggedValue}
              snappingPoints={[100]}
              height={height + 100}
              friction={0.4}
              >
              <View style={styles.panel}>
              <View style={styles.panelHeader} >
                <Text>Slide Up</Text>
              </View>
                <View style={styles.bottomContainer}>
                  <View>
                    <Card title="Keterangan Lengkap" image={{ uri: 'https://www.api.ilmusaya.web.id/storage/foto/'+this.state.foto }} >
                      <Text h3>
                        {this.state.namaTempat}
                      </Text>
                      <Divider/>
                      <Text>Jarak Tempuh {this.state.informasi.jarak} Km</Text>
                      <Text>Durasi Tempuh {this.state.informasi.durasi} Menit</Text>
                      <Text> {this.state.keterangantempat} </Text>
                    </Card>
                  </View>
                </View>
              </View>
              </SlidingUpPanel>
            :
              <View></View>
            }
            
          {/* </View> */}
        </View>
      </View>
    );
  }
}


const styles = StyleSheet.create({
    wrapper:{
        flex:1,
        backgroundColor:'black',
        paddingTop: Platform.OS === 'ios' ? 0 : StatusBar.currentHeight 

    },
    container: {
        flex: 1,
        // paddingTop: 25,
        backgroundColor: '#fff',
        
    },
    map: {
        position: 'absolute',
        top: 1,
        left: 0,
        right: 0,
        bottom: 0,
        zIndex:-1
    },
    actionButtonIcon: {
        fontSize: 20,
        height: 22,
        color: 'white',
      },
      bottomContainer:{
        flex: 1,
        backgroundColor: "#f8f9fa",
        alignItems: "center",
      },
      panel: {
        flex: 1,
        backgroundColor: "white",
        position: "relative"
      },
      panelHeader: {
        height: 5,
        backgroundColor: "#00D8CB",
        justifyContent: "center",
        alignItems: 'center',
        padding: 16,
      },
      textHeader: {
        fontSize: 28,
        color: "#FFF"
      },
});
